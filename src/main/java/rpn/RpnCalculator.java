package rpn;

import java.util.Stack;

public class RpnCalculator {
    private int accumulator;
    private Stack<Integer> stack = new Stack<>();

    public Integer getAccumulator() { return accumulator; }

    public void setAccumulator(int accumulator) { this.accumulator = accumulator; }

    public Integer getStack() { return stack.peek(); }

    public void enter() { stack.push(accumulator); }

    public void plus() { accumulator += stack.pop(); }

    public void minus() { accumulator = stack.pop() - accumulator; }

    public void multiply() { accumulator *= stack.pop(); }

    public int evaluate(String expression) {
        String strEvaluate = expression.replaceAll("\\s","");

        for(int i = 0; i < strEvaluate.length(); i++) {
            char c = strEvaluate.charAt(i);
            if (Character.isDigit(c)) {
                this.setAccumulator(Character.getNumericValue(c));
            } else {
                if (c == '+') { this.plus(); }
                if (c == '-') { this.minus(); }
                if (c == '*') { this.multiply(); }
            }
            if (i+1 != strEvaluate.length() && Character.isDigit(strEvaluate.charAt(i+1))) { this.enter(); }
        }
        return this.getAccumulator();
    }
}
