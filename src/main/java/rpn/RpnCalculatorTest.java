package rpn;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void canSetAccumulator() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);

        assertThat(c.getAccumulator(), is(1));
    }

    @Test
    public void calculatorEntersAccumulatorToStack() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();

        assertThat(c.getStack(), is(1));
    }

    @Test
    public void calculatorAddsNumbers() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();

        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSubtractsNumbers() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(5);
        c.enter();
        c.setAccumulator(3);
        c.minus();

        assertThat(c.getAccumulator(), is(2));
    }

    @Test
    public void calculatorMultipliesNumbers() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(5);
        c.enter();
        c.setAccumulator(3);
        c.multiply();

        assertThat(c.getAccumulator(), is(15));
    }

    @Test
    public void calculatorAddsThenMultipliesNumbers() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();

        assertThat(c.getAccumulator(), is(12));
    }

    @Test
    public void calculatorMakesMultipleCalculations() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();

        assertThat(c.getAccumulator(), is(21));
    }

    @Test
    public void calculatorEvaluatesStringAsExpression() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.evaluate("1 2 +"), is(3));
        assertThat(c.evaluate("1 2 + 4 *"), is(12));
        assertThat(c.evaluate("5 1 2 + 4 * + 3 +"), is(20));
    }
}

