package refactoring;

import java.util.*;

public class Refactoring {

    public int incrementInvoiceNumber() {
        return ++invoiceNumber;
    }

    public void addFilledOrdersTo(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);
        printInvoiceRows(invoiceRows);
        printValue(calculateInvoiceTotal(invoiceRows));
    }

    private Double calculateInvoiceTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    public String getItemsAsHtml() {
        String result = "<ul>";
        for(String item : Arrays.asList(items.get(0), items.get(1), items.get(2), items.get(3))){
            result += createTag("li", item);
        }
        result += "</ul>";
        return result;
    }

    private String createTag(String tag, String data) {
        return "<" + tag + ">" + data + "</" + tag + ">";
    }

    public boolean isSmallOrder() {
        return (order.getTotal() > 100);
    }
    
    public void printPrice() {

        double basePrice = getBasePrice();
        System.out.println("Price not including VAT: " + basePrice);
        System.out.println("Price including VAT: " + basePrice * VAT);
    }

    private static final double VAT = 1.2;

    public void calculatePayFor(Job job) {
        final boolean onHolidayNight = (job.day == 6 || job.day == 7)
                && (job.hour > 20 || job.hour < 7);

        if (onHolidayNight)  {
        }
    }

    public boolean canAccessResource(SessionData sessionData) {
        return (isAdmin(sessionData) && hasPreferredStatus(sessionData));
    }

    private boolean isAdmin(SessionData sessionData) {
        return sessionData.getCurrentUserName().equals("Admin")
                || sessionData.getCurrentUserName().equals("Administrator");
    }

    private boolean hasPreferredStatus(SessionData sessionData) {
        return sessionData.getStatus().equals("preferredStatusX")
                || sessionData.getStatus().equals("preferredStatusY");
    }

    public void drawLines() {
        Space space = new Space();
        space.drawLine(line1startPoint, line1endPoint);
        space.drawLine(line2startPoint, line2endPoint);
    }

    private StartPoint line1startPoint = new StartPoint(12, 3, 5);
    private EndPoint line1endPoint = new EndPoint(2, 4, 6);
    private StartPoint line2startPoint = new StartPoint(2, 4, 6);
    private EndPoint line2endPoint = new EndPoint(0, 1, 0);

    public int calculateWeeklyPayWithOvertime(int hoursWorked) {
        double overtimeRate = 1.5 * hourRate;

        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);

        int straightPay = straightTime * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);

        return straightPay + overtimePay;
    }

    public int calculateWeeklyWithoutOvertime(int hoursWorked) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        return (straightTime + overTime) * hourRate;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        incrementInvoiceNumber();
        addFilledOrdersTo(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(StartPoint startPoint, EndPoint endPoint) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
