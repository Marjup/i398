package stack;

public class Stack {
    private Integer[] numbers;
    private int pointer = 0;

    public Stack(int size) {
        numbers = new Integer[size];
    }

    public int getSize() {
        return pointer;
    }

    public void push(int i) {
        numbers[pointer] = i;
        pointer++;
    }

    public int pop() {
        if (pointer == 0) {
            throw new IllegalStateException();
        }

        return numbers[--pointer];
    }

    public int peek() {
        if (pointer == 0) {
            throw new IllegalStateException();
        }
        return numbers[pointer - 1];
    }
}
