package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void newStack_addTwoElements() {
        Stack stack = new Stack(100);

        stack.push(3);
        stack.push(9);

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void newStack_takeTwoElements() {
        Stack stack = new Stack(100);

        stack.push(12);
        stack.push(14);

        int firstPoppedElement = stack.pop();
        int secondPoppedElement = stack.pop();

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void stackHasTwoElements_TakeTwoElements_AreSameElements() {
        Stack stack = new Stack(100);

        stack.push(20);
        stack.push(30);

        int firstPoppedElement = stack.pop();
        int secondPoppedElement = stack.pop();

        assertThat(firstPoppedElement, is(30));
        assertThat(secondPoppedElement, is(20));
    }

    @Test
    public void stackHasTwoElements_PeekLastElement() {
        Stack stack = new Stack(100);

        stack.push(50);
        stack.push(60);

        int peekedElement = stack.peek();

        assertThat(peekedElement, is(60));

    }

    @Test
    public void stackHasTwoElements_PeekLastElement_HasTwoElements() {
        Stack stack = new Stack(100);

        stack.push(50);
        stack.push(60);

        int peekedElement = stack.peek();

        assertThat(stack.getSize(), is(2));

    }

    @Test
    public void stackHasTwoElements_PeekLastElementTwice() {
        Stack stack = new Stack(100);

        stack.push(50);
        stack.push(60);

        int peekedElement = stack.peek();
        int peekedElement2 = stack.peek();

        assertThat(peekedElement, is(60));
        assertThat(peekedElement2, is(60));

    }

    @Test
    public void newStackHasNoElements_popElement_throwsIllegalStateException() {
        Stack stack = new Stack(100);

        assertThrows(IllegalStateException.class, () -> { stack.pop(); });

    }

    @Test
    public void newStackHasNoElements_peekElement_throwsIllegalStateException() {
        Stack stack = new Stack(100);

        assertThrows(IllegalStateException.class, () -> { stack.peek(); });

    }
}

