package mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockitoSimple {

    private BankService mockBankService = mock(BankService.class);

    @Test
    public void transferSuccessScenario() {
        TransferService transferService = new TransferService(mockBankService);
        when(mockBankService.getBalance("1")).thenReturn(10);
        transferService.transferMoney(2, "1", "3");
        verify(mockBankService).transfer(2, "1", "3");
    }

    @Test
    public void transferringNegativeAmountFails() {
        TransferService transferService = new TransferService(mockBankService);
        transferService.transferMoney(-1, "1", "2");
        verify(mockBankService, never()).transfer(-1, "1", "2");
    }

    @Test
    public void transferFailsWhenNotEnoughFunds() {
        TransferService transferService = new TransferService(mockBankService);
        when(mockBankService.getBalance("1")).thenReturn(10);
        transferService.transferMoney(12, "1", "2");
        verify(mockBankService, never()).transfer(12, "1", "2");
    }

}

interface BankService {

    int getBalance(String formAccount);

    void transfer(int amount, String formAccount, String toAccount);

}

class TransferService {

    private BankService bankService;

    public TransferService(BankService bankService) {
        this.bankService = bankService;
    }

    public void transferMoney(int amount, String formAccount, String toAccount) {

        if (amount <= 0 || formAccount.equals(toAccount))
            return;

        if (bankService.getBalance(formAccount) >= amount) {
            bankService.transfer(amount, formAccount, toAccount);
        }
    }
}
