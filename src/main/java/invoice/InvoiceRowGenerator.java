package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class InvoiceRowGenerator {

    public List<InvoiceRow> generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd) {
        validateInput(amount, periodStart, periodEnd);

        List<InvoiceRow> invoicerows = new ArrayList<>();
        int numberOfMonths = getNumberOfMonths(periodStart, periodEnd);

        BigDecimal[] amounts = generateAmounts(amount, numberOfMonths);
        List<LocalDate> dates = generateDates(numberOfMonths, periodStart);

        for (int i = 0; i < amounts.length; i++) {
            invoicerows.add(new InvoiceRow(amounts[i], dates.get(i)));

        }
        return invoicerows;
    }

    private void validateInput(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd){
        if(periodStart == null || periodEnd == null || amount == null) throw new IllegalArgumentException();
        if(periodStart.isAfter(periodEnd)) throw new IllegalArgumentException();
        if(amount.equals(BigDecimal.valueOf(0))) throw new IllegalArgumentException();
    }

    private BigDecimal[] generateAmounts(BigDecimal amount, int numberOfMonths) {
        int size = numberOfMonths;
        if(amount.compareTo(BigDecimal.valueOf(3)) == -1) return new BigDecimal[] {amount};

        BigDecimal[] amountAndRemainder = amount.divideAndRemainder(BigDecimal.valueOf(size));

        if(amountAndRemainder[0].compareTo(BigDecimal.valueOf(3)) == -1){
            amountAndRemainder = amount.divideAndRemainder(BigDecimal.valueOf(3));
            size = amountAndRemainder[0].intValue();
        }

        BigDecimal[] amounts = new BigDecimal[size];
        BigDecimal dividedAmount = amountAndRemainder[0];
        BigDecimal remainder = amountAndRemainder[1];

        for (int i = 0; i < amounts.length; i++) {
            if(dividedAmount.compareTo(BigDecimal.valueOf(3)) == -1) dividedAmount = BigDecimal.valueOf(3);
            amounts[i] = dividedAmount;
        }

        if(!remainder.equals(BigDecimal.valueOf(0))){
            BigDecimal[] newDivide = remainder.divideAndRemainder(BigDecimal.valueOf(2));
            amounts[amounts.length-1] = amounts[amounts.length-1].add(newDivide[0]);
            amounts[amounts.length-2] = amounts[amounts.length-2].add(newDivide[0]);
            if(!newDivide[1].equals(BigDecimal.valueOf(0))) {
                amounts[amounts.length-1] = amounts[amounts.length-1].add(newDivide[1]);
            }
        }

        return amounts;
    }

    private List<LocalDate> generateDates(int numberOfDates, LocalDate periodStart){
        List<LocalDate> dates = new ArrayList<>();
        LocalDate dateOfInvoice = periodStart;
        for (int i = 0; i < numberOfDates; i++) {
            if(i != 0) {
                LocalDate newDate = dateOfInvoice.plusMonths(1L);
                dateOfInvoice = LocalDate.of(newDate.getYear(), newDate.getMonth(), 1);
            }
            dates.add(dateOfInvoice);
        }
        return dates;
    }

    private int getNumberOfMonths(LocalDate startDate, LocalDate endDate){
        int months = 0;
        Period timePeriod = startDate.until(endDate);
        if(startDate.getDayOfMonth() > endDate.getDayOfMonth()) {
            months = timePeriod.getMonths() + 2;
        } else {
            months =  timePeriod.getMonths() + 1;
        }
        return months;
    }
}
