package invoice;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

public class InvoiceRowGeneratorTest {

    @Mock InvoiceRowDao dao;

    @Test
    public void shouldgenerateRightAmountOfInvoiceRowsBasedByThePeriod(){
        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-11-01");
        LocalDate periodEnd = asDate("2018-01-02");
        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(3));
    }

    @Test
    public void periodStartDateisTheFirstInvoiceDate() {
        BigDecimal amount = new BigDecimal(10);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-02-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertEquals(periodStart, generatedInvoiceRows.get(0).date);
    }

    @Test
    public void allInvoiceDatesAreOnTheFirstOfTheMonthBesidesTheFirstInvoice(){
        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-01-03");
        LocalDate periodEnd = asDate("2017-03-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.get(0).date.getDayOfMonth(), is(3));
        assertThat(generatedInvoiceRows.get(1).date.getDayOfMonth(), is(1));
        assertThat(generatedInvoiceRows.get(2).date.getDayOfMonth(), is(1));

    }

    @Test
    public void periodStartDateIsNull_throws() {
        BigDecimal amount = new BigDecimal(10);
        LocalDate periodStart = null;
        LocalDate periodEnd = asDate("2017-02-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();

        assertThrows(IllegalArgumentException.class, () -> {
            List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        });
    }

    @Test
    public void periodEndDateIsNull_throws() {
        BigDecimal amount = new BigDecimal(10);
        LocalDate periodStart = asDate("2017-02-02");
        LocalDate periodEnd = null;

        InvoiceRowGenerator generator = new InvoiceRowGenerator();

        assertThrows(IllegalArgumentException.class, () -> {
            List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        });
    }

    @Test
    public void periodStartDateIsAfterPeriodEndDate_throws() {
        BigDecimal amount = new BigDecimal(10);
        LocalDate periodStart = asDate("2017-02-02");
        LocalDate periodEnd = asDate("2017-01-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();

        assertThrows(IllegalArgumentException.class, () -> {
            List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        });
    }

    @Test
    public void generateInvoiceRowsBasedOnAPeriod() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-02-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(2));
        assertThat(generatedInvoiceRows.get(0).date.toString(), is("2017-01-01"));
        assertThat(generatedInvoiceRows.get(1).date.toString(), is("2017-02-01"));
    }

    @Test
    public void amountIsNull_throws() {
        BigDecimal amount = null;
        LocalDate periodStart = asDate("2017-02-02");
        LocalDate periodEnd = asDate("2017-01-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();

        assertThrows(IllegalArgumentException.class, () -> {
            List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        });
    }

    @Test
    public void amountIsZero_throws() {
        BigDecimal amount = new BigDecimal(0);
        LocalDate periodStart = asDate("2017-02-02");
        LocalDate periodEnd = asDate("2017-01-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();

        assertThrows(IllegalArgumentException.class, () -> {
            List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        });
    }

    @Test
    public void amountDividesEquallyBetweenAPeriod() {
        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(3));
        assertThat(generatedInvoiceRows.get(0).amount, is(new BigDecimal(3)));
        assertThat(generatedInvoiceRows.get(1).amount, is(new BigDecimal(3)));
        assertThat(generatedInvoiceRows.get(2).amount, is(new BigDecimal(3)));

    }

    @Test
    public void amountDividesNotEquallyBetweenAPeriod() {
        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(3));
        assertThat(generatedInvoiceRows.get(0).amount, is(new BigDecimal(3)));
        assertThat(generatedInvoiceRows.get(1).amount, is(new BigDecimal(4)));
        assertThat(generatedInvoiceRows.get(2).amount, is(new BigDecimal(4)));
    }

    @Test
    public void putTogetherPeriodAmountsWhenPeriodAmountIsLessThan3(){
        BigDecimal amount = new BigDecimal(7);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-04-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(2));
        assertThat(generatedInvoiceRows.get(0).amount, is(new BigDecimal(3)));
        assertThat(generatedInvoiceRows.get(1).amount, is(new BigDecimal(4)));
    }

    @Test
    public void generateOneInvoiceRowWhenAmountIsLessThan3(){
        BigDecimal amount = new BigDecimal(2);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> generatedInvoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);

        assertThat(generatedInvoiceRows.size(), is(1));
        assertThat(generatedInvoiceRows.get(0).amount, is(new BigDecimal(2)));
    }

    @Test
    public void saveInvoiceRowToDbWithRightAmount() {
        dao.save(new InvoiceRow(new BigDecimal(1), LocalDate.now()));

        verify(dao).save(argThat(getMatcherForAmount(1)));
    }

    @Test
    public void saveInvoiceRowToDbWithRightDate() {
        dao.save(new InvoiceRow(new BigDecimal(1), asDate("2017-01-01")));

        verify(dao).save(argThat(getMatcherForDate(asDate("2017-01-01"))));
    }

    @Test
    public void saveMultipleRowsToDb() {
        dao.save(new InvoiceRow(new BigDecimal(1), LocalDate.now()));
        dao.save(new InvoiceRow(new BigDecimal(1), LocalDate.now()));

        verify(dao, times(2)).save(argThat(getMatcherForAmount(1)));
    }

    @Test
    public void dontSaveToDbWhenInvoiceRowNull() {
        SaveService saveService = new SaveService(dao);
        saveService.saveInvoice(null);

        verify(dao, never()).save(null);

    }

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {
        // Example matcher for testing that argument has certain amount.

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                String message = MessageFormat.format(
                        "InvoiceRow with amount {0}", amount);

                description.appendText(message);
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForDate(final LocalDate date){

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return date.equals(item.date);
            }

            @Override
            public void describeTo(Description description) {
                String message = MessageFormat.format(
                        "InvoiceRow with date {0}", date);

                description.appendText(message);
            }
        };
    }

    private LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }

}
