package invoice;

public class SaveService {

    private InvoiceRowDao invoiceRowDao;

    public SaveService(InvoiceRowDao invoiceRowDao) {
        this.invoiceRowDao = invoiceRowDao;
    }

    public void saveInvoice(InvoiceRow row) {
        if(row == null) {
            return;
        }

        invoiceRowDao.save(row);
    }


}
