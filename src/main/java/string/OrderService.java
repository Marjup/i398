package string;

import java.util.*;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        List<Order> filledOrders = new ArrayList<>();
        for(Order order: dataSource.getOrders()) {
            if(order.isFilled()) filledOrders.add(order);
        }
        return filledOrders;
    }

    public List<Order> getOrdersOver(double amount) {
        List<Order> ordersOver = new ArrayList<>();

        for(Order order: dataSource.getOrders()) {
            if(order.getTotal() > amount) ordersOver.add(order);
        }
        return ordersOver;

    }

    public List<Order> getOrdersSortedByDate() {
        throw new IllegalStateException("not implemented");
    }
}
