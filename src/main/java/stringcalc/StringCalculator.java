package stringcalc;

public class StringCalculator {
    public int add(String inputString) {
        if ("".equals(inputString)) {
            return 0;
        }

        if (inputString == null) {
            throw new IllegalArgumentException();
        }

        int sum = 0;

        for (String numb: inputString.split(", ")) {
            sum += Integer.parseInt(numb);
        }

        return sum;

    }
}

