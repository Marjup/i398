package stringcalc;

import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException

    @Test
    public void emptyString_returnsZero() {
        StringCalculator calc = new StringCalculator();
        int result = calc.add("");
        assertThat(result, is(0));
    }

    @Test
    public void stringWithNumber_returnsNumber() {
        StringCalculator calc = new StringCalculator();
        int result = calc.add("5");
        assertThat(result, is(5));
    }

    @Test
    public void stringWithMultipleNumbers_returnsSumOfNumbers() {
        StringCalculator calc = new StringCalculator();
        int result = calc.add("1, 5");
        assertThat(result, is(6));
    }

    @Test
    public void null_returnsIllegalArgumentException() {
        StringCalculator calc = new StringCalculator();
        assertThrows(IllegalArgumentException.class, () -> { calc.add(null); });

    }
}

