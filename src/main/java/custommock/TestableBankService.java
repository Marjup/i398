package custommock;

import common.BankService;
import common.Money;

public class TestableBankService implements BankService {

    private Money moneyWithdraw;
    private Money moneyDeposit;
    private String toAccount;
    private String fromAccount;
    private boolean withdrawCalled = false;
    private boolean fundsAvailable = true;

    public boolean wasWithdrawCalledWith(Money money, String account) {
        if (moneyWithdraw.equals(money) && fromAccount.equals(account)) { return true; }

        return false;
    }

    public boolean wasDepositCalledWith(Money money, String account) {
        if (moneyDeposit.equals(money) && toAccount.equals(account)) { return true; }

        return false;

    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        if (hasSufficientFundsFor(money, fromAccount)) {
            withdrawCalled = true;
            this.moneyWithdraw = money;
            this.fromAccount = fromAccount;
        }
    }

    @Override
    public void deposit(Money money, String toAccount) {
        this.moneyDeposit = money;
        this.toAccount = toAccount;
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return fundsAvailable;
    }

    public void setSufficientFundsAvailable(boolean areFundsAvailable) {
        fundsAvailable = areFundsAvailable;
    }

    public boolean wasWithdrawCalled() {
        return withdrawCalled;
    }

}