package gol;

import com.sun.deploy.util.StringUtils;

public class Frame {

    private int[][] frame;

    public Frame(int width, int height) {
        if(width < 1 || height < 1) { throw new IllegalArgumentException(); }
        frame = new int[height][width];
    }

    public static Frame parseFrame(String frameString){
        String[] arr = StringUtils.splitString(frameString, "\n");
        String[] arr2 = StringUtils.splitString(arr[0], " ");

        Frame frame = new Frame(arr.length, arr2.length);

        for (int i = 0; i < arr.length; i++) {
            String[] splitedString = StringUtils.splitString(arr[i], " ");
            for (int j = 0; j < splitedString.length; j++) {
                if(splitedString[j].equals("X")) frame.markAlive(i, j);
            }
        }

        return frame;
    }

    @Override
    public String toString() {
        StringBuilder framestr = new StringBuilder();
        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j < frame[i].length ; j++) {
                if(isAlive(i, j)) {
                    framestr.append("X ");
                } else {
                    framestr.append("- ");
                }
            }
            framestr.append("\n");
        }
        return framestr.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Frame)) { return false; }
        Frame frame = (Frame) obj;
        if(this.getWidth() != frame.getWidth() || this.getHeight() != frame.getHeight()) { return false; }

        for (int i = 0; i < frame.getHeight(); i++) {
            for (int j = 0; j < frame.getWidth(); j++) {
                if (!(this.frame[i][j] == frame.frame[i][j])) { return false; }
            }
        }

        return true;
    }

    public Integer getNeighbourCount(int x, int y) {
        int count = 0;

        for(int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (i == x && j == y) continue;
                if (isCellInFrame(i, j) && isAlive(i, j)) count++;
            }
        }
        return count;
    }

    public boolean isAlive(int x, int y) {
        if(!isCellInFrame(x, y)) {throw new IllegalArgumentException(); }
        return frame[x][y] == 1;
    }

    public void markAlive(int x, int y) {
        if(!isCellInFrame(x, y)) {throw new IllegalArgumentException(); }
        frame[x][y] = 1;
    }

    public Frame nextFrame() {
        Frame nextFrame = new Frame(this.getWidth(), this.getHeight());
        for (int i = 0; i < nextFrame.getHeight(); i++) {
            for (int j = 0; j < nextFrame.getWidth(); j++) {
                if(this.isAlive(i, j)){
                    if(getNeighbourCount(i, j) == 2 || getNeighbourCount(i, j) == 3) nextFrame.markAlive(i, j);
                } else {
                    if(getNeighbourCount(i, j) == 3){ nextFrame.markAlive(i, j); }
                }

            }
        }
        return nextFrame;
    }

    public int getWidth() {
        return frame[0].length;
    }

    public int getHeight() {
        return frame.length;
    }

    public boolean isCellInFrame(int x, int y) {
        if (x < 0 || y < 0) { return false; }
        if (x >= getHeight() || y >= getWidth()) { return false;}
        return true;
    }
}