package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


public class GameOfLifeTest {

    @Test
    public void makeFrameWithGivenWidthAndHeight() {
        Frame frame = new Frame(1, 2);

        assertEquals(frame.getWidth(), 1);
        assertEquals(frame.getHeight(), 2);
    }

    @Test
    public void makeFrameWithWidthOrHeightlessThan1_throws(){
        assertThrows(IllegalArgumentException.class, () -> {
             new Frame(0, -1);
        });
    }

    @Test
    public void checkIfCellBelongsToAFrame() {
        Frame frame = new Frame(2, 3);

        assertTrue(frame.isCellInFrame(1, 0));
        assertFalse(frame.isCellInFrame(4, 1));
        assertFalse(frame.isCellInFrame(-1, 0));
    }

    @Test
    public void cellIsAliveWhenMarkedAlive() {
        Frame frame = new Frame(2, 3);
        frame.markAlive(0, 1);

        assertTrue(frame.isAlive(0, 1));
    }

    @Test
    public void cellNotInFrameMarkAlive_throws() {
        Frame frame = new Frame(2, 3);

        assertThrows(IllegalArgumentException.class, () -> {
            frame.markAlive(-1, 0);
        });
    }

    @Test
    public void checkIsAliveWhenCellNotInFrame_throws() {
        Frame frame = new Frame(2, 3);

        assertThrows(IllegalArgumentException.class, () -> {
            frame.isAlive(-1, 0);
        });
    }

    @Test
    public void printStringRepresentationOfAFrame() {
        Frame frame = new Frame(2, 2);
        frame.markAlive(0, 1);
        String expectedString = "- X \n" +
                                "- - \n";

        assertEquals(expectedString, frame.toString());
    }

    @Test
    public void parsingStringToFrame() {
        String frameString = "- X - \n" +
                             "- - - \n" +
                             "X X - \n";
        Frame frame = Frame.parseFrame(frameString);

        assertEquals(frameString, frame.toString());
    }

    @Test
    public void countCellNeighbours() {
        String frameString = "- - - \n" +
                             "X - X \n" +
                             "X X X \n";
        Frame frame = Frame.parseFrame(frameString);
        int neighbours = frame.getNeighbourCount(1, 1);

        assertThat(neighbours, is(5));
    }

    @Test
    public void countCellNotInFrameNeighbours() {
        String frameString = "- X \n" +
                             "- - \n";
        Frame frame = Frame.parseFrame(frameString);
        int neighbours = frame.getNeighbourCount(1, -1);

        assertThat(neighbours, is(0));
    }

    @Test
    public void nextFrameHasTheSameSizeAsOldFrame(){
        Frame frame = new Frame(1, 2);
        Frame nextFrame = frame.nextFrame();

        assertThat(nextFrame.getWidth(), is(1));
        assertThat(nextFrame.getHeight(), is(2));
    }

    @Test
    public void cellWithLessThanTwoNeighboursDiesNextFrame() {
        String frameString = "X - \n" +
                             "- - \n";
        Frame frame = Frame.parseFrame(frameString);
        Frame nextFrame = frame.nextFrame();

        assertFalse(nextFrame.isAlive(0, 0));
    }

    @Test
    public void cellWith2or3NeighboursStaysAliveNextFrame() {
        String frameString = "X X \n" +
                             "X - \n";
        Frame frame = Frame.parseFrame(frameString);
        Frame nextFrame = frame.nextFrame();

        assertTrue(nextFrame.isAlive(0, 0));
    }

    @Test
    public void cellWithOver3NeighboursIsDeadNextFrame() {
        String frameString = "X X - \n" +
                             "X X X \n" +
                             "- X - \n";
        Frame frame = Frame.parseFrame(frameString);
        Frame nextFrame = frame.nextFrame();

        assertFalse(nextFrame.isAlive(1, 1));
    }

    @Test
    public void cellWith3NeighboursIsAliveNextFrame() {
        String frameString = "X X \n" +
                             "X - \n";
        Frame frame = Frame.parseFrame(frameString);
        Frame nextFrame = frame.nextFrame();

        assertTrue(nextFrame.isAlive(1, 1));
    }

    @Test
    public void checkIfTwoFramesAreEqual() {
        String frameString = "X - \n" +
                             "X X \n";
        Frame frame1 = Frame.parseFrame(frameString);
        Frame frame2 = Frame.parseFrame(frameString);

        assertEquals(frame1, frame2);
    }

    @Test
    public void programCalculatesNextFrameCorrectly() {
        String frameString = "- X - - - - \n" +
                             "- - X X - - \n" +
                             "- X X - - - \n" +
                             "- - - - - - \n";
        Frame frame = Frame.parseFrame(frameString);

        String expectedNextFrameString = "- - X - - - \n" +
                                         "- - - X - - \n" +
                                         "- X X X - - \n" +
                                         "- - - - - - \n";

        String expectedNextNextFrameString = "- - - - - - \n" +
                                             "- X - X - - \n" +
                                             "- - X X - - \n" +
                                             "- - X - - - \n";
        Frame expectedNextFrame = Frame.parseFrame(expectedNextFrameString);
        Frame expectedNextNextFrame = Frame.parseFrame(expectedNextNextFrameString);

        Frame nextFrame = frame.nextFrame();
        Frame nextNextFrame = nextFrame.nextFrame();

        assertEquals(nextFrame, expectedNextFrame);
        assertEquals(nextNextFrame, expectedNextNextFrame);
    }
}