package person;

public class PersonBuilder {
    private String name = "Alison";
    private int age = 25;
    private String gender = Person.GENDER_FEMALE;
    private Address address = new Address("Park Avenue", "New York");

    public PersonBuilder withAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder withTown(String town) {
        this.address.setTown(town);
        return this;
    }

    public Person build() {
        Person person = new Person(name, age, gender, address);
        return person;
    }
}
