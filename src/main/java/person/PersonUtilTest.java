package person;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.Arrays.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class PersonUtilTest {

    private PersonUtil personUtil = new PersonUtil();

    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
    }

    @Test
    public void findOldestPersonWithNullList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getOldest(null);
        });
    }

    @Test
    public void findOldestPersonWithEmptyList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getOldest(asList());
        });
    }

    @Test
    public void findsPersonsInLegalAge() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(15).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)), is(asList(p1, p3)));
    }

    @Test
    public void findPersonsInLegalAgeWithNullList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getPersonsInLegalAge(null);
        });
    }

    @Test
    public void findPersonsInLegalAgeWithEmptyList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getPersonsInLegalAge(asList());
        });
    }

    @Test
    public void findsWomen() {
        Person p1 = aPerson().withGender(Person.GENDER_FEMALE).build();
        Person p2 = aPerson().withGender(Person.GENDER_MALE).build();
        Person p3 = aPerson().withGender(Person.GENDER_MALE).build();

        assertThat(personUtil.getWomen(asList(p1, p2, p3)), is(asList(p1)));
    }

    @Test
    public void findWomenWithNullList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getWomen(null);
        });
    }

    @Test
    public void findWomenWithEmptyList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getWomen(asList());
        });
    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().withTown("Paris").build();
        Person p2 = aPerson().withTown("London").build();
        Person p3 = aPerson().withTown("London").build();

        assertThat(personUtil.getPersonsWhoLiveIn("London", asList(p1, p2, p3)), is(asList(p2, p3)));
    }

    @Test
    public void findPersonsLivingInSpecifiedTownWithNull_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getPersonsWhoLiveIn(null, null);
        });
    }

    @Test
    public void findPersonsLivingInSpecifiedTownWithEmptyList_throws() {
        assertThrows(IllegalArgumentException.class, () -> {
            personUtil.getPersonsWhoLiveIn("London", asList());
        });
    }

    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}
